import { Component } from '@angular/core';
import { AuthenticationService } from '../services/authentication';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  constructor(private authenticationService: AuthenticationService) { }

  async login() {
    this.authenticationService.login();
  }

  register() {
    this.authenticationService.register();
  }
}
