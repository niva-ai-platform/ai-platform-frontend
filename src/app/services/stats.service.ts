import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StatsService {

  constructor(private httpClient: HttpClient) { }

  backendURL = 'https://niva-ai-dev.tssg.org/api/v1/';

  getResultsPassed() {
    return this.httpClient.get(this.backendURL + 'results/ai-results-passed-count', { observe: 'response' })
  }

  getResultsRejected() {
    return this.httpClient.get(this.backendURL + 'results/ai-results-rejected-count', { observe: 'response' })
  }

  getRegisteredModels() {
    // return this.httpClient.get(this.backendURL + 'registry/module-registry', { observe: 'response' })
    return this.httpClient.get('https://niva-ai-dev.tssg.org/ai-registry/list', { observe: 'response' })

  }

  getPassedCountByModel(modelName: string) {
    return this.httpClient.get(this.backendURL + 'results/ai-results-passed-count/' + modelName, { observe: 'response' })
  }

  getRejectedCountByModel(modelName: string) {
    return this.httpClient.get(this.backendURL + 'results/ai-results-rejected-count/' + modelName, { observe: 'response' })
  }
}
