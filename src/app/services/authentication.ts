import { Injectable } from "@angular/core";
import { KeycloakService } from "keycloak-angular";

export let keycloakInstance: any;
export function initialiseKeyCloak(keycloak: KeycloakService) {
  return async () =>
    keycloakInstance = await keycloak.init({
      config: {
        url: 'https://niva-ai-keycloak-dev.tssg.org',
        realm: 'niva-ai',
        clientId: 'niva-ai-frontend',
      },
      initOptions: {
        redirectUri: 'https://niva-ai-dev.tssg.org/*',
        // this will solved the error
        checkLoginIframe: false,
      },
      // https://stackoverflow.com/questions/71382837/angular-keycloak-is-not-adding-the-bearer-token-by-default-to-my-http-requests
      // enableBearerInterceptor: true,
      // bearerExcludedUrls: [],
      // bearerPrefix:'Bearer '
    });
}

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private keyCloak: KeycloakService) { }

  token = '';

  async login() {
    await this.keyCloak.login({
      redirectUri: 'https://niva-ai-dev.tssg.org/main',
    });
  }
  async register() {
    await this.keyCloak.register({
      redirectUri: 'https://niva-ai-dev.tssg.org/main',
    })
  }

  logout() {
    this.keyCloak.logout(window.location.origin);
  }

  async getToken() {
    await this.keyCloak.getToken().then(response => {
      this.token = response;
    })
  }

  isAuthenticated() {
    if (keycloakInstance.authenticated === undefined) {
      return false;
    } else {
      return keycloakInstance.authenticated;
    }
  }
}
