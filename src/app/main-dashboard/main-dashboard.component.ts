import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../services/authentication';
import {Router, NavigationEnd,ActivatedRoute} from '@angular/router';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-main-dashboard',
  templateUrl: './main-dashboard.component.html',
  styleUrls: ['./main-dashboard.component.scss']
})
export class MainDashboardComponent implements OnInit {
  constructor(private httpClient: HttpClient, private auth: AuthenticationService) { }

  ngOnInit(): void {
    this.auth.getToken();
    this.httpClient.get('https://niva-ai-dev.tssg.org/ai-registry/list').subscribe(response => {
      this.aiModels = response;
    })
    for (let i = 1; i <= 100; i++) {
      this.confidenceArray.push(i)
    }
    this.chosenConfidence = 85;
  }

  aiModels: any = [];
  aiUrl = new FormControl([]);
  selectedFiles = [];
  uploadedImages: any = [];
  dbImage: any = [];
  successResponse: string = '';
  loading = false;
  appId: string = '';
  results: boolean = false;
  images: any = [];
  apiResultArray: any = [];
  confidenceArray: any = [];
  chosenConfidence: any;

  resetState() {
    this.dbImage =[]
    this.uploadedImages=[]
    this.apiResultArray = []
    this.aiUrl = new FormControl([]);
   }

  onSelect(event: any) {
    this.uploadedImages.push(...event.addedFiles);
  }

  onRemove(event: any) {
    this.uploadedImages.splice(this.uploadedImages.indexOf(event), 1);
  }

  async imageUploadAction() {
    this.loading = true;
    const imageFormData = new FormData();

    Object.keys(this.uploadedImages).forEach(key => {
      imageFormData.append('files', this.uploadedImages[key]);
      imageFormData.append('appId', this.appId);
    });

    const endPoint = 'https://niva-ai-dev.tssg.org/image-fs/file_upload/' + this.appId

    await this.httpClient.post(endPoint, imageFormData, { observe: 'response' })
      .subscribe((response) => {
        if (response.status === 200) {
          const postResponse = JSON.stringify(response);
          const parsed = JSON.parse(postResponse);
          this.images = parsed.body.uploadedFiles;
          for (const item of this.images) {
            const url = item + "?token=" + this.auth.token;
            this.dbImage.push(url);
          }
          this.loading = false;
        } else {
          this.successResponse = 'Image not uploaded due to some error!';
        }
      }
      );
  }
  async runAIModel() {
    this.loading = true;
    const imageProcessing = [];
    const preprocessing = [];
    const processing = [];
    const models = this.aiUrl.value as string[];
    for (const model of models) {
      if (model === 'Detect People' || model ===  'Detect License Plates'){
        preprocessing.push({
          name: model,
          onFail: 'continue',
          onSuccess: 'continue',
          allowUpdateImage: true,
          confidenceOverride: this.chosenConfidence / 100,
        });
      } else if (model === 'Detect Image Quality'){
        imageProcessing.push({
          name: model,
          onFail: 'continue',
          onSuccess: 'continue',
          allowUpdateImage: true,
          confidenceOverride: this.chosenConfidence / 100,
        });
      } else {
        processing.push({
          name: model,
          onFail: 'continue',
          onSuccess: 'continue',
          allowUpdateImage: true,
          confidenceOverride: this.chosenConfidence / 100,
        });
      }
    }
    const stages = [ ...imageProcessing, ...preprocessing, ...processing]

    const body = {
      images: this.images,
      stages: stages
    }

    await this.httpClient.post("https://niva-ai-dev.tssg.org/ai-registry/run-pipeline", body, { observe: 'response' })
      .subscribe(response => {
        if (response.status === 200) {
          let AIreposponse = JSON.stringify(response)
          let parsed = JSON.parse(AIreposponse)
          let parsedAIResponse: any[] = [];
          parsedAIResponse = [...parsedAIResponse, parsed.body]
          parsedAIResponse.forEach((item: any) => {
            for (const i of item) {
              const url = i.metadata.outputFileUrl + "?token=" + this.auth.token;
              i.metadata.outputFileUrl = url;
              this.apiResultArray.push(i)
            }
          });
    //       /* let body = parsed.body;
    //       let b = body.body
    //       this.parsedAIResponse = [...this.parsedAIResponse, b]
    //       this.parsedAIResponse.forEach((item: any) => {
    //         console.log(item)
    //         const url = item.metadata.outputFileUrl + "?token=" + this.auth.token;
    //         item.metadata.outputFileUrl = url;
    //       }) */
          this.loading = false;
        }
        else {
          this.loading = false;
        }
      });
    /* await this.dbImage.forEach(async (imagePath: string) => {
      this.runAIModel(imagePath);
    });
    console.log(this.parsedAIResponse) */
  }

  /* async runAIModel(imagePathName: string) {

    setTimeout(() => {
      let AIEndPoint = 'https://niva-ai-dev.tssg.org/detect-people/start-ai-process/' + imagePathName;
      console.log(AIEndPoint)
      this.httpClient.get(AIEndPoint, { observe: 'response' })
        .subscribe((response) => {
          if (response.status === 200) {
            let AIreposponse = JSON.stringify(response)
            let parsed = JSON.parse(AIreposponse)
            this.parsedAIResponse = [...this.parsedAIResponse, parsed.body]
            this.parsedAIResponse.forEach((item: any) => {
              console.log(JSON.stringify(item.metadata.outputFileUrl))
              const url = item.metadata.outputFileUrl + "?token=" + this.auth.token;
              item.metadata.outputFileUrl = url;
            })
            this.loading = false;
          }
          else {
            this.loading = false;
          }
        })
    }, 3000)

  }*/

  // viewImage() {
  //   this.httpClient.get('http://localhost:8080/get/image/info/' + this.image)
  //     .subscribe(
  //       res => {
  //         this.postResponse = res;
  //         this.dbImage = 'data:image/jpeg;base64,' + this.postResponse.image;
  //       }
  //     );
  // }
}
