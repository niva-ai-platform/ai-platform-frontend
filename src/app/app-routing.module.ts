import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { HelpComponent } from './help/help.component';
import { HomeComponent } from './home/home.component';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from './services/authGuard';
import { SharedComponent } from './shared/shared.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  {
    path: 'main',
    component: SharedComponent,
    children: [{ path: '', component: MainDashboardComponent }],
    canActivate: [AuthGuard],
  },
  {
    path: 'about',
    component: SharedComponent,
    children: [{ path: '', component: AboutComponent }],
    canActivate: [AuthGuard],
  },
  {
    path: 'help',
    component: SharedComponent,
    children: [{ path: '', component: HelpComponent }],
    canActivate: [AuthGuard],
  },
  {
    path: 'profile',
    component: SharedComponent,
    children: [{ path: '', component: ProfileComponent }],
    canActivate: [AuthGuard],
  },

  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
