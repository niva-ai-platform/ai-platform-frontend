import { Component, ViewChild } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Chart, ChartData, ChartOptions, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';
import { StatsService } from '../services/stats.service';
import { lastValueFrom } from 'rxjs';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import {Context} from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-stats-section',
  templateUrl: './stats-section.component.html',
  styleUrls: ['./stats-section.component.scss']
})
export class StatsSectionComponent {

  constructor(private observer: BreakpointObserver, private statsService: StatsService) {}

  @ViewChild(BaseChartDirective) chart?: BaseChartDirective;

  maxChartHeight = 300;
  maxChartWidth = 600;
  totalCountStats: number[] = []
  totalCountChartType: ChartType = 'doughnut';
  totalCountChartLabels: string[] = ['Passed', 'Rejected'];
  totalCountChartData: ChartData<'doughnut'> = {
    labels: this.totalCountChartLabels,
    datasets: [
    { data: this.totalCountStats, backgroundColor: 'green'},
  ]};

  countsByModelStats: number[][] = [];
  countsByModelChartType: ChartType = 'bar';
  countsByModelChartLabels: string[] = [];
  countsByModelChartOptions: ChartOptions = {
    indexAxis: 'y',
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      x: {
        stacked: true
      },
      y: {
        stacked: true
      }
    },
  };
  countsByModelChartData: ChartData<'bar'> = {
    labels: this.countsByModelChartLabels,
    datasets: [
      { data: [], label: 'Passed', backgroundColor: 'green' },
      { data: [], label: 'Rejected' }
    ],
  };

  ngOnInit() {
    Chart.register(ChartDataLabels);
    Chart.defaults.set('plugins.datalabels',
    { color: 'white',
      display: (ctx: Context) => {
        return ctx.dataset.data[ctx.dataIndex] !== 0;
      }
    })
    this.updateTotalCountStats();
    this.updateCountsByModelStats();
  }

  ngAfterViewInit() {
    this.observer.observe(['(max-width: 1148px)', '(max-width: 1020px)', '(max-width: 880px)']).subscribe((res) => {
      if (!res.matches) {
        this.maxChartWidth = 600;
      } else if (res.breakpoints['(max-width: 880px)']) {
        this.maxChartWidth = 300;
      } else if (res.breakpoints['(max-width: 1020px)']) {
        this.maxChartWidth = 400;
      } else if (res.breakpoints['(max-width: 1148px)']) {
        this.maxChartWidth = 500;
      } else {
        this.maxChartWidth = 600;
      }
    });
  }

  updateTotalCountStats() {
    this.statsService.getResultsPassed().subscribe((response) => {
      if (response.status === 200) {
        const postResponse = JSON.stringify(response);
        const parsed = JSON.parse(postResponse);
        this.totalCountStats[0] = parsed.body.data;
        this.updateTotalCountChart();
      } else {
        // this.successResponse = 'Image not uploaded due to some error!';
      }
    })

    this.statsService.getResultsRejected().subscribe((response) => {
      if (response.status === 200) {
        const postResponse = JSON.stringify(response);
        const parsed = JSON.parse(postResponse);
        this.totalCountStats[1] = parsed.body.data;
        this.updateTotalCountChart();
      } else {
        // this.successResponse = 'Image not uploaded due to some error!';
      }
    })
  }

  async updateCountsByModelStats() {
    let updatedPassedCount: number[] = [];
    let updatedRejectedCount: number[] = [];

    const response = this.statsService.getRegisteredModels();
    const resolvedResponse = await lastValueFrom(response);
    const strigifiedResponse = JSON.stringify(resolvedResponse);
    const parsed = JSON.parse(strigifiedResponse);
    this.countsByModelChartLabels = parsed.body.map((module: any) => module.name).sort((a: string, b: string) => a < b ? -1 : (a > b ? 1 : 0));

    const retrieveCountFunction = async (countType: string, argument: string) => {
      let response;
      if (countType === 'passed') {
        response = this.statsService.getPassedCountByModel(argument);
      } else {
        response = this.statsService.getRejectedCountByModel(argument);
      }
      const resolvedResponse = await lastValueFrom(response);
      const strigifiedResponse = JSON.stringify(resolvedResponse);
      const parsed = JSON.parse(strigifiedResponse);
      return parsed.body.data;
    };

    for (let model of this.countsByModelChartLabels) {

      const passedCount = await retrieveCountFunction('passed', model);
      updatedPassedCount.push(passedCount);

      const rejectedCount = await retrieveCountFunction('rejected', model);
      updatedRejectedCount.push(rejectedCount);
    }

    this.updateCountsByModelChart(this.countsByModelChartLabels, updatedPassedCount, updatedRejectedCount);
  }


  updateTotalCountChart() {
    this.totalCountChartData = {
      labels: this.totalCountChartLabels,
      datasets: [
      { data: this.totalCountStats, backgroundColor: ['#00B050', '#4a566e'] },
    ]};
    this.chart?.update();
  }

  updateCountsByModelChart(updatedLabels: string[], updatedPassedCount: number[], updatedRejectedCount: number[]) {
    this.countsByModelChartData.labels = this.countsByModelChartLabels;
    this.countsByModelChartData = {
        labels: updatedLabels,
        datasets: [
          { data: updatedPassedCount, label: 'Passed', backgroundColor: '#00B050'  },
          { data: updatedRejectedCount, label: 'Rejected', backgroundColor: '#4a566e' }
        ],
      };
    this.chart?.update();
  }

  updateChartDimensions() {
    return {'min-height': `${this.maxChartHeight}px`, 'width': `${this.maxChartWidth}px`};
  }

  getBarChartDivWidth() {
    return {'width': `${this.maxChartWidth}px`};
  }
}
